﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OntoEntities
{
    public enum DataType
    {
        Bit = 1,
        Long = 2,
        Double = 3,
        DateTime = 4,
        String = 5
    }
    public enum EntityRole
    {
        Class = 0,
        Object = 1,
        RelationType = 2
    }
    public class ClusterEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string NameShort { get; set; }
        public List<string> ParentIds { get; set; }

        public List<string> SearchHints { get; set; }

        public bool? BoolVal { get; set; }

        public DateTime? DateTimeVal { get; set; }

        public double? DoubleVal { get; set; }

        public long? LongVal { get; set; }

        public DataType? DataType { get; set; }

        public EntityRole? EntityRole { get; set; }

        public bool RootEntity { get; set; }

        public ClusterEntity Clone()
        {
            var cloneItem = new ClusterEntity();

            cloneItem.Id = this.Id;
            cloneItem.Name = this.Name;
            cloneItem.LongVal = this.LongVal;
            cloneItem.ParentIds = this.ParentIds.Select(parId => parId).ToList();
            cloneItem.RootEntity = this.RootEntity;
            cloneItem.SearchHints = this.SearchHints.Select(searchHint => searchHint).ToList();
            cloneItem.BoolVal = this.BoolVal;
            cloneItem.DataType = this.DataType;
            cloneItem.DateTimeVal = this.DateTimeVal;
            cloneItem.DoubleVal = this.DoubleVal;
            cloneItem.EntityRole = this.EntityRole;

            return cloneItem;
        }

    }
}
