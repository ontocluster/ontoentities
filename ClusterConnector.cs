﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoEntities
{
    public enum ConnectorType
    {
        Full = 0,
        Partial = 1,
        ClassAttribute = 2,
        Attribute = 3
    }
    public class ClusterConnector
    {
        public string IdLeft { get; set; }

        public string IdParentLeft { get; set; }

        public ClusterEntity ClusterEntityLeft { get; set; }

        public EntityRole? EntityRoleLeft { get; set; }

        public string IdRight { get; set; }

        public string IdParentRight { get; set; }

        public EntityRole? EntityRoleRight { get; set; }

        public ClusterEntity ClusterEntityRight { get; set; }

        public string IdRelation { get; set; }

        public List<string> SearchHints { get; set; }

        public ClusterEntity ClusterEntityRelation { get; set; }

        public ConnectorType? ConnectorType { get; set; }

        public long? WeightPrimary { get; set; }
        public long? WeightSecondary { get; set; }

        public long? WeightSecondaryBack { get; set; }

    }
}
