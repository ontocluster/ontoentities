﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoEntities.DataClasses
{
    public class ClassTreeItem
    {
        public ClusterEntity Class { get; set; }
        public List<ClassTreeItem> SubClasses { get; set; }
        public ClassTreeItem ParentTreeItem { get; set; }

        public string ClassNamePath { get; set; }
        public string ClassIdPath { get; set; }

    }
}
