﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoEntities.DataClasses
{
    public class LogStates
    {
        
        private Classes classes = new Classes();
        public ClusterEntity Success { get; private set; }
        public ClusterEntity Delete { get; private set; }

        public ClusterEntity Error { get; private set; }

        public ClusterEntity Exists { get; private set; }

        public ClusterEntity Insert { get; private set; }

        public ClusterEntity Nothing { get; private set; }

        public ClusterEntity Relation { get; private set; }

        public ClusterEntity Update { get; private set; }

        public List<ClusterEntity> LogStateEntities { get; private set; }

        public LogStates()
        {

            Delete = new ClusterEntity
            {
                Id = "bb6a95553af640fc9fb0489d2678dff2",
                Name = "Delete",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };

            Error = new ClusterEntity
            {
                Id = "cc71434176314b78b8f4385db073635f",
                Name = "Error",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };

            Exists = new ClusterEntity
            {
                Id = "0b285306f64d4444bffe627a21687eff",
                Name = "Exist",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };

            Insert = new ClusterEntity
            {
                Id = "a6df6ab2359045b1b32535334a2f574a",
                Name = "Insert",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };

            
            Nothing = new ClusterEntity
            {
                Id = "95666887fb2a416e9624a48d48dc5446",
                Name = "Nothing",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };
            Relation = new ClusterEntity
            {
                Id = "a46b74723c8e44a8b7853913b760db",
                Name = "Relation",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };
            Success = new ClusterEntity
            {
                Id = "84251164265e4e0294b2ed7c40a02e56",
                Name = "Success",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };

            Update = new ClusterEntity
            {
                Id = "2bf7e9d6fb9c40929b16ecc4fef7c072",
                Name = "Update",
                ParentIds = new List<string> { classes.Logstate.Id },
                EntityRole = EntityRole.Object,
                DataType = DataType.String
            };

            LogStateEntities = new List<ClusterEntity>
            {
                Delete,
                Error,
                Exists,
                Insert,
                Nothing,
                Relation,
                Success,
                Update
            };
        }
    }
}
