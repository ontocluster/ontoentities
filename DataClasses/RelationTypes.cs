﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoEntities.DataClasses
{
    public class RelationTypes
    {
        public ClusterEntity Contains { get; private set; }
        public ClusterEntity BelongingAttribute { get; private set; }
        public ClusterEntity BelongingRelationType { get; private set; }
        public ClusterEntity BelongingClass { get; private set; }
        public ClusterEntity BelongingObject { get; private set; }
        public ClusterEntity Belonging { get; private set; }
        public ClusterEntity BelongingResource { get; private set; }
        public ClusterEntity BelongingsTo { get; private set; }
        public ClusterEntity IsOfType { get; private set; }
        public ClusterEntity Apply { get; private set; }
        public ClusterEntity Src { get; private set; }
        public ClusterEntity Dst { get; private set; }

        public List<ClusterEntity> RelationTypeEntities { get; private set; }

        public RelationTypes()
        {
            RelationTypeEntities = new List<ClusterEntity>();

            Contains = new ClusterEntity
            {
                Id = "e971160347db44d8a476fe88290639a4",
                Name = "contains",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(Contains);

            BelongingAttribute = new ClusterEntity
            {
                Id = "81bbd380e35648a1a4b7fdbaebe7273c",
                Name = "belonging Attribute",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(BelongingAttribute);

            BelongingClass = new ClusterEntity
            {
                Id = "f2b54f82ada5460eafe5551d55629f14",
                Name = "belonging Class",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(BelongingClass);

            BelongingRelationType = new ClusterEntity
            {
                Id = "4417582dbd6347fbab18770a611917fe",
                Name = "belonging RelationType",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(BelongingRelationType);

            BelongingObject = new ClusterEntity
            {
                Id = "f68a9438fb8b418d8e0bd9aefc9ecdf3",
                Name = "belonging Object",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(BelongingObject);

            Belonging = new ClusterEntity
            {
                Id = "796712399c8f493cb5e749700f9543f4",
                Name = "belonging",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(Belonging);

            BelongingResource = new ClusterEntity
            {
                Id = "92619f7ecbf342308ca34b7e7e8883f6",
                Name = "belonging Resource",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(BelongingResource);

            IsOfType = new ClusterEntity
            {
                Id = "9996494aef6a4357a6ef71a92b5ff596",
                Name = "is of Type",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(IsOfType);

            Apply = new ClusterEntity
            {
                Id = "70a11e6243cb41de89f93f681abdee9d",
                Name = "Apply",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(Apply);

            Src = new ClusterEntity
            {
                Id = "0820e2c523fe4450986961bb58dc1c22",
                Name = "src",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(Src);

            Dst = new ClusterEntity
            {
                Id = "339cf6dce131466cb7b71857f75bb5eb",
                Name = "dst",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(Dst);

            BelongingsTo = new ClusterEntity
            {
                Id = "e07469d9766c443e85266d9c684f944f",
                Name = "belongs to",
                EntityRole = EntityRole.RelationType
            };
            RelationTypeEntities.Add(BelongingsTo);
        }
    }
}
