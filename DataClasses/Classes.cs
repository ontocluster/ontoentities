﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoEntities.DataClasses
{
    public class Classes
    {
        public List<ClusterEntity> ClassItems { get; private set; }
        public ClusterEntity Root { get; private set; }

        public ClusterEntity AttributeType { get; private set; }
        public ClusterEntity System { get; private set; }
        public ClusterEntity Logstate { get; private set; }

        public ClusterEntity Directions { get; private set; }

        public ClusterEntity Ontologies { get; private set; }
        public ClusterEntity OntologyItems { get; private set; }

        public ClusterEntity OntologyRelationRule { get; set; }

        public ClusterEntity OntologyJoin { get; set; }

        public ClusterEntity OntologyMapping { get; set; }

        public ClusterEntity OntologyMappingItem { get; set; }

        public ClusterEntity MappingRule { get; set; }

        public ClusterEntity Server { get; set; }

        public ClusterEntity OntologyItemCreationRule { get; set; }

        public ClusterEntity Module { get; set; }

        public ClusterEntity ModuleFunction { get; set; }

        public ClusterEntity OntologyNamingRule { get; set; }

        public ClusterEntity Variable { get; set; }

        public Classes()
        {
            ClassItems = new List<ClusterEntity>();
            Root = new ClusterEntity { Id = "49fdcd27e1054770941d7485dcad08c1", Name = "Root", EntityRole = EntityRole.Class, DataType = DataType.String};
            ClassItems.Add(Root);
            System = new ClusterEntity { Id = "665dd88b792e4256a27a68ee1e10ece6", Name = "System", EntityRole = EntityRole.Class, ParentIds = new List<string> { Root.Id }, DataType = DataType.String };
            ClassItems.Add(System);
            AttributeType = new ClusterEntity { Id = "CE30FF0DD007409F93D974BF3B0C5430", Name = "AttributeType", EntityRole = EntityRole.Class, ParentIds = new List<string> { Root.Id }, DataType = DataType.String };
            ClassItems.Add(System);
            Logstate = new ClusterEntity { Id = "1d9568afb6da49908f4d907dfdd30749", Name = "Logstate", EntityRole = EntityRole.Class, ParentIds = new List<string> { System.Id }, DataType = DataType.String };
            ClassItems.Add(Logstate);
            Directions = new ClusterEntity { Id = "3d1dc6cfb96449869808f39b7c5c3907", Name = "Direction", EntityRole = EntityRole.Class, ParentIds = new List<string> { System.Id }, DataType = DataType.String };
            ClassItems.Add(Directions);
            Ontologies = new ClusterEntity { Id = "eb411e2ff93d4a5ebbbac0b5d7ec0197", Name = "Ontologies", EntityRole = EntityRole.Class, ParentIds = new List<string> { System.Id }, DataType = DataType.String };
            ClassItems.Add(Ontologies);
            OntologyItems = new ClusterEntity { Id = "d3f72a683f6146a48ff381db05997dc8", Name = "Ontology-Items", EntityRole = EntityRole.Class, ParentIds = new List<string> { Ontologies.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyItems);
            OntologyRelationRule = new ClusterEntity { Id = "925f489dec8d4130a418fcb022a4c731", Name = "Ontology-Relation-Rule", EntityRole = EntityRole.Class, ParentIds = new List<string> { Ontologies.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyRelationRule);
            OntologyJoin = new ClusterEntity { Id = "aab30dd04faf4386896016218132b110", Name = "Ontology-Join", EntityRole = EntityRole.Class, ParentIds = new List<string> { Ontologies.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyJoin);
            OntologyMapping = new ClusterEntity { Id = "7392dc3336e6422e96d90764a852cec3", Name = "Ontology-Mapping", EntityRole = EntityRole.Class, ParentIds = new List<string> { Ontologies.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyMapping);
            OntologyMappingItem = new ClusterEntity { Id = "4d84e06af18046deb89d562cef0ca858", Name = "Ontology-Mapping-Item", EntityRole = EntityRole.Class, ParentIds = new List<string> { OntologyMapping.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyMappingItem);
            MappingRule = new ClusterEntity { Id = "8a2ab30ee0764b6b8d18150066a95eda", Name = "Mapping-Rule", EntityRole = EntityRole.Class, ParentIds = new List<string> { OntologyMapping.Id }, DataType = DataType.String };
            ClassItems.Add(MappingRule);
            Server = new ClusterEntity { Id = "d7a03a35875142b48e0519fc7a77ee91", Name = "Server", EntityRole = EntityRole.Class, ParentIds = new List<string> { System.Id }, DataType = DataType.String };
            ClassItems.Add(Server);
            OntologyItemCreationRule = new ClusterEntity { Id = "e4e7e7621df04b81a6ff8f8d67cf988a", Name = "OntologyItem-Creation-Rules", EntityRole = EntityRole.Class, ParentIds = new List<string> { OntologyItems.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyItemCreationRule);
            OntologyItemCreationRule = new ClusterEntity { Id = "aa616051e5214facabdbcbba6f8c6e73", Name = "Module", EntityRole = EntityRole.Class, ParentIds = new List<string> { System.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyItemCreationRule);
            Module = new ClusterEntity { Id = "aa616051e5214facabdbcbba6f8c6e73", Name = "Module", EntityRole = EntityRole.Class, ParentIds = new List<string> { System.Id }, DataType = DataType.String };
            ClassItems.Add(Module);
            ModuleFunction = new ClusterEntity { Id = "86b05e853ce24a56be3c80375a686a82", Name = "Module-Function", EntityRole = EntityRole.Class, ParentIds = new List<string> { Module.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyItemCreationRule);
            OntologyNamingRule = new ClusterEntity { Id = "5abc8144b51f469ea9ff2883295b6f26", Name = "Ontology-Naming-Rule", EntityRole = EntityRole.Class, ParentIds = new List<string> { Ontologies.Id }, DataType = DataType.String };
            ClassItems.Add(OntologyNamingRule);
            Variable = new ClusterEntity { Id = "4158aad2656a4fb997bf524c6f5fecaa", Name = "Variable", EntityRole = EntityRole.Class, ParentIds = new List<string> { System.Id }, DataType = DataType.String };
            ClassItems.Add(Variable);
        }
    }
}
